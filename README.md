# Python script for RIVM codes distribution
This script automagically distributes the booster activation codes to clients
in the required format.

Usage:
`python distribute.py [start row] [codes csv] [input csv]`

- `[start row]`: The first row with a valid code (the very first row is row 1)
- `[codes csv]`: The csv file with all the codes
- `[input csv]`: The input csv file, containing the email addresses and the number of codes and stuff

Example:
`python distribute.py 14322 codes.csv input.csv`

The script produces two files:
- `output.csv`, which is a copy of `input.csv`, but with the codes appended in columns to the right
- `codes_remaining.csv`, which contains unused codes.

## For windows users
You can run `run.bat` by double-clicking. 
It will essentially run `python distribute.py 0 codes.csv input.csv`.
This will take the list of codes in `codes.csv` and the `input.csv` and produce
an `output.csv` based on the input and the codes.
The PowerShell window will remain open to see what the first and last codes were.
