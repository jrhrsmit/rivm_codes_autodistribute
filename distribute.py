import sys
import csv

codes_start_row = 0
codes_filename = "codes.csv"

input_filename = "input.csv"
input_col_num_codes = 3

output_filename = "output.csv"
codes_remaining_filename = "codes_remaining.csv"

MAX_CODES_PER_ROW = 250


def main():
    if len(sys.argv) != 4:
        print("Usage:")
        print("\tpython distribute.py [start row] [codes csv] [input csv]")
        print("Options:")
        print("\tstart row: The first row with a valid code (the very first row is row 1)")
        print("\tcodes csv: The csv file with all the codes")
        print("\tinput csv: The input csv file, containing the email addresses and the number of codes and stuff")
        print("Example:")
        print("\tpython distribute.py 14322 codes.csv input.csv")
        sys.exit(1)

    first_code = ""
    last_code = ""

    codes_start_row = int(sys.argv[1])
    codes_filename = sys.argv[2]
    input_filename = sys.argv[3]

    codes = []
    codes_used = 0
    with open(codes_filename, newline='') as csvfile:
        codes_reader = csv.reader(csvfile, delimiter=';', quoting=csv.QUOTE_NONE)
        r = 0
        for row in codes_reader:
            r += 1
            if r >= codes_start_row:
                codes.append(row[0])

    # open the input and output files
    with open(input_filename, newline='') as csvfile:
        input_reader = csv.reader(csvfile, delimiter=';', quoting=csv.QUOTE_NONE)
        with open(output_filename, 'w', newline='') as csvfile2:
            output_writer = csv.writer(csvfile2, delimiter=';', quoting=csv.QUOTE_NONE)
            # skip the header
            # append header with column names for codes
            row = next(input_reader)
            for col in range(0, MAX_CODES_PER_ROW + 2):
                row.append(f"CustomField{col + 2}")
            # write header
            output_writer.writerow(row)
            # write the other lines
            csvfile.seek(0)
            next(csvfile)
            for row in input_reader:
                print(
                    f"{int(row[input_col_num_codes])} codes for {row[0:-1]}")
                base = row.copy()
                codes_requested = int(row[input_col_num_codes])
                # keep count of how many codes we used
                codes_used += int(codes_requested)
                # append codes to row
                for i in range(0, codes_requested):
                    # keep track of the first and last used code
                    if first_code == "":
                        first_code = codes[0]
                    last_code = codes[0]
                    # add a code
                    row.append(codes.pop(0))
                    # write the (last) row for this client
                    if i == codes_requested - 1:
                        row[input_col_num_codes] = int(
                            row[input_col_num_codes]) % MAX_CODES_PER_ROW
                        for j in range(0, MAX_CODES_PER_ROW - int(i % MAX_CODES_PER_ROW) - 1):
                            row.append("")
                        row.append(int(i / MAX_CODES_PER_ROW + 1))
                        row.append(
                            int(codes_requested / MAX_CODES_PER_ROW + 1))
                        output_writer.writerow(row)
                    # max 250 codes per row, multiple rows for clients with
                    # >250 codes
                    elif (i + 1) % (MAX_CODES_PER_ROW) == 0:
                        row[input_col_num_codes]=MAX_CODES_PER_ROW
                        row.append(int(i / MAX_CODES_PER_ROW + 1))
                        row.append(
                            int(codes_requested / MAX_CODES_PER_ROW + 1))
                        output_writer.writerow(row)
                        row=base.copy()

    # give some feedback about how many and which codes we used
    print(f"Used {codes_used} codes.")
    print(f"First code used: \t{first_code}")
    print(f"Last code used: \t{last_code}")

    # write remaining codes for future use
    with open(codes_remaining_filename, 'w', newline='') as csvfile:
        codes_remaining_writer=csv.writer(csvfile)
        for code in codes:
            codes_remaining_writer.writerow([code])


if __name__ == "__main__":
    sys.exit(main())
